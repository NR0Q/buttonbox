#include <iostream>
#include <fstream>
#include <bitset>
#include <cstdlib>
#include <csignal>
#include <linux/uinput.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <cerrno>
#include <boost/asio.hpp>
#include "include/json.hpp"

bool keepRunning = true;
using json = nlohmann::json;
using namespace std;
boost::asio::io_service io;
boost::asio::serial_port serial(io);
int uinput_fd = -1; // Global uinput device file descriptor

void closeVirtualDevice() {
    if (uinput_fd != -1) {
        // Destroy the virtual input device
        ioctl(uinput_fd, UI_DEV_DESTROY);
        close(uinput_fd);
    }
}

int doExit(int ret) {
    closeVirtualDevice();
    serial.close();
    return ret;
}

char* bitArrayToHexCharArray(const bitset<12>& bits) {
    // Convert the bitset to a string representation
    string binaryString = bits.to_string();

    // Initialize a char array of length 3
    char* hexArray = new char[4];

    // Convert the binary string to a hexadecimal string
    string hexString = "";
    for (int i = 0; i < 12; i += 4) {
        // Extract 4 bits at a time and convert to hex
        bitset<4> nibble(binaryString.substr(i, 4));
        int hexValue = nibble.to_ulong();
        char hexChar = (hexValue < 10) ? ('0' + hexValue) : ('A' + hexValue - 10);
        hexString += hexChar;
    }

    // Copy the hexadecimal string to the char array
    strcpy(hexArray, hexString.c_str());

    return hexArray;
}

bitset<12> hexStringToBinary(const string& hexStr) {
    // Create a stringstream to convert the hexadecimal string to an integer.
    stringstream ss;
    ss << hex << hexStr;

    unsigned int hexValue;
    ss >> hexValue;

    // Convert the integer to a 12-bit binary value.
    bitset<12> binaryValue(hexValue);

    return binaryValue;
}

bitset<12> invertBitArray(const bitset<12>& bits) {
    return ~bits;
}


string convertToString(char* a, int size)
{
    int i;
    string s = "";
    for (i = 0; i < size; i++) {
        s = s + a[i];
    }
    return s;
}

void applyMaskReplace(bitset<12>& original, const bitset<12>& mask) {
    // Apply the mask and replace the original bitset.
    original = original ^ mask;
}


void turnOffBits(bitset<12>& original, const bitset<12>& mask) {
    // Use the bitwise AND assignment operator to turn off bits in the original bitset
    original &= ~mask;
    
}
json readJsonFile(const string& filename) {
    // Create an input file stream and open the JSON file.
    ifstream file(filename);
    if (!file.is_open()) {
        throw runtime_error("Failed to open JSON file: " + filename);
        doExit(2);
    }

    // Read the content of the file into a JSON object.
    json jsonData;
    file >> jsonData;

    // Close the file.
    file.close();

    return jsonData;
}

json getObjectByKeyValue(const json& jsonObject, const string& keyToMatch) {
    for (const auto& item : jsonObject.items()) {
        if (item.key() == keyToMatch) {
            return item.value();
        }
    }
    return json(); // Return an empty JSON object if the key-value pair is not found.
}

void focusWindowWithTitle(const string& titleSubstring) {
    // Create a command to list all windows with "GridTracker" in the title.
    string listCommand = "wmctrl -l | grep '" + titleSubstring + "'";

    // Execute the command and capture the output.
    FILE* fp = popen(listCommand.c_str(), "r");
    if (fp == nullptr) {
        cerr << "Failed to execute the command." << endl;
        doExit(1);
    }

    char buffer[128];
    string windowInfo;
    while (fgets(buffer, sizeof(buffer), fp) != nullptr) {
        windowInfo = buffer; // Capture the window info.
        break; // Only consider the first window found.
    }

    pclose(fp);

    // Extract the window ID from the output.
    size_t firstSpace = windowInfo.find(' ');
    if (firstSpace != string::npos) {
        string windowId = windowInfo.substr(0, firstSpace);
        
        // Create a command to focus the window by its ID.
        string focusCommand = "wmctrl -i -a " + windowId;

        // Execute the focus command to bring the window to the front.
        if (system(focusCommand.c_str()) == 0) {
            cout << "Focused window with title containing '" << titleSubstring << "'" << endl;
        } else {
            cerr << "Failed to focus the window." << endl;
        }
    } else {
        cerr << "No window found with title containing '" << titleSubstring << "'" << endl;
    }
}

bool initializeVirtualDevice() {
    // Open the uinput device for writing
    uinput_fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if (uinput_fd == -1) {
        cerr << "Unable to open /dev/uinput: " << strerror(errno) << endl;
        return false;
    }

    // Initialize the input device structure
    struct uinput_setup usetup;
    memset(&usetup, 0, sizeof(usetup));
    usetup.id.bustype = BUS_USB;
    usetup.id.vendor = 0x1234; // Vendor ID
    usetup.id.product = 0x5678; // Product ID
    strcpy(usetup.name, "Sample UInput Device");

    // Create the virtual input device
    ioctl(uinput_fd, UI_SET_EVBIT, EV_KEY);
    // Add other event types as needed
    // Example: ioctl(uinput_fd, UI_SET_EVBIT, EV_REL); // Relative movement
    // ...

    // Set up key bits (you can add more keys as needed)
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_1);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_2);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_3);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_4);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_5);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_6);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_7);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_8);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_9);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_0);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_A);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_B);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_C);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_D);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_E);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_F);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_G);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_H);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_I);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_J);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_K);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_L);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_M);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_N);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_O);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_P);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_Q);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_R);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_S);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_T);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_U);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_V);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_W);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_X);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_Y);
    ioctl(uinput_fd, UI_SET_KEYBIT, KEY_Z);

    ioctl(uinput_fd, UI_DEV_SETUP, &usetup);
    ioctl(uinput_fd, UI_DEV_CREATE);

    return true;
}

int charToKey(char keyChar) {
    switch (keyChar) {
        case '1':
            return KEY_1;
            break;
        case '2':
            return KEY_2;
            break;
        case '3':
            return KEY_3;
            break;
        case '4':
            return KEY_4;
            break;
        case '5':
            return KEY_5;
            break;
        case '6':
            return KEY_6;
            break;
        case '7':
            return KEY_7;
            break;
        case '8':
            return KEY_8;
            break;
        case '9':
            return KEY_9;
            break;
        case '0':
            return KEY_0;
            break;
        case 'A':
            return KEY_A;
            break;
        case 'B':
            return KEY_B;
            break;
        case 'C':
            return KEY_C;
            break;
        case 'D':
            return KEY_D;
            break;
        case 'E':
            return KEY_E;
            break;
        case 'F':
            return KEY_F;
            break;
        case 'G':
            return KEY_G;
            break;
        case 'H':
            return KEY_H;
            break;
        case 'I':
            return KEY_I;
            break;
        case 'J':
            return KEY_J;
            break;
        case 'K':
            return KEY_K;
            break;
        case 'L':
            return KEY_L;
            break;
        case 'M':
            return KEY_M;
            break;
        case 'N':
            return KEY_N;
            break;
        case 'O':
            return KEY_O;
            break;
        case 'P':
            return KEY_P;
            break;
        case 'Q':
            return KEY_Q;
            break;
        case 'R':
            return KEY_R;
            break;
        case 'S':
            return KEY_S;
            break;
        case 'T':
            return KEY_T;
            break;
        case 'U':
            return KEY_U;
            break;
        case 'V':
            return KEY_V;
            break;
        case 'W':
            return KEY_W;
            break;
        case 'X':
            return KEY_X;
            break;
        case 'Y':
            return KEY_Y;
            break;
        case 'Z':
            return KEY_Z;
            break;
    }
    return 0;
}

bool sendKeyPressRelease(int key) {
    if (uinput_fd == -1) {
        cerr << "Virtual device not initialized." << endl;
        return false;
    }

    // Simulate a key press event
    struct input_event event_press;
    event_press.type = EV_KEY;
    event_press.code = key;
    event_press.value = 1; // Key press
    write(uinput_fd, &event_press, sizeof(event_press));

    // Simulate a key release event
    struct input_event event_release;
    event_release.type = EV_KEY;
    event_release.code = key;
    event_release.value = 0; // Key release
    write(uinput_fd, &event_release, sizeof(event_release));

    return true;
}

int sendButtonLightSerialString(string buttonRow, bitset<12> bits) {
    char* hexChars;
    hexChars = bitArrayToHexCharArray(invertBitArray(bits));
    string data = "~"+ buttonRow + convertToString(hexChars,3) + "\r";
    cout << "Output to serial port: " + data << endl;
    boost::asio::write(serial, boost::asio::buffer(data));
    delete[] hexChars;
    return 0;
}

int toggleButtonLights(string buttonRow, bitset<12>& rowBits, bitset<12> buttonMask) {
    applyMaskReplace(rowBits, buttonMask);
    cout << "Row " + buttonRow + ": " + rowBits.to_string() + " button mask: " + buttonMask.to_string() << endl;
    sendButtonLightSerialString(buttonRow, rowBits);
    return 0;
}

int momentaryButtonLights(string buttonRow, bitset<12>& rowBits, bitset<12> buttonMask) {
    applyMaskReplace(rowBits, buttonMask);
    cout << "Row " + buttonRow + ": " + rowBits.to_string() + " button mask: " + buttonMask.to_string() << endl;
    sendButtonLightSerialString(buttonRow, rowBits);
    applyMaskReplace(rowBits, buttonMask);
    usleep(100000);
    sendButtonLightSerialString(buttonRow, rowBits);
    return 0;
}

int groupButtonLights(string buttonRow, bitset<12>& rowBits, bitset<12> buttonMask, bitset<12> groupMask) {
    turnOffBits(rowBits, groupMask);
    applyMaskReplace(rowBits, buttonMask);
    sendButtonLightSerialString(buttonRow, rowBits);
    cout << "Row " + buttonRow + ": " + rowBits.to_string() + " button mask: " + buttonMask.to_string() << endl;
    return 0;
}

bool initializeSerialPort(string portName) {
    try {
        // Open the serial port.
        serial.open(portName);
        serial.set_option(boost::asio::serial_port_base::baud_rate(9600)); // Set the baud rate as needed.
    } catch (exception& e) {
        cerr << "Error: " << e.what() << endl;
        return false;
    }
    return true;
}

void signalHandler(int signum) {
    cout << "Received signal " << signum << ". Cleaning up and exiting." << endl;
    
    doExit(0);
    
    // Set the flag to exit the loop
    keepRunning = false;
}

int main() {

    // Register signal handlers for SIGINT and SIGTERM
    signal(SIGINT, signalHandler); // Ctrl+C
    signal(SIGTERM, signalHandler); // Termination signal

    json configData;
    try {
        string filename = "buttonBox.json"; // Replace with your JSON file path.

        configData = readJsonFile(filename);

        // Now you can work with the JSON data in the 'jsonData' object.
        cout << "JSON data: " << configData.dump(4) << endl;
    } catch (const exception& e) {
        cerr << "Error: " << e.what() << endl;
        doExit(2);
    }

    string portName = "/dev/ttyUSB0";
    bitset<12> defaultRow("000000000000");
    bitset<12> row1Bits("000000000000");
    bitset<12> row2Bits("000000000000");
    bitset<12> rowFBits("000000000000");
    
    cout << "Enter serial port [/dev/ttyUSB0] :";
    cin >> portName;

    if (!initializeSerialPort(portName)) {
        doExit(5);
    }

    if (!initializeVirtualDevice()) {
        doExit(13);
    }

    while (keepRunning) {  
        string receivedData;
        string buttonRow;
        string buttonVal;
        bitset<12> buttonMask("000000000000");
        json buttonConfig;
        
        char c;
        int crCount = 0; // Counter for carriage returns

        while (crCount < 2) {
            boost::asio::read(serial, boost::asio::buffer(&c, 1));
            receivedData += c;

            if (c == '\r') {
                crCount++;
            }
        }
        // cout << receivedData << endl;
        if (receivedData.substr(0,1) == "~") {
            buttonVal = receivedData.substr(2,2) + receivedData.substr(5,1);
            buttonRow = receivedData.substr(1,1);
            if (buttonVal == "FFF") {
                // keyup, do nothing for now
            } else if (buttonRow == "4") {
                // junk, do nothing for now
            } else {
                if (configData.contains(buttonRow + buttonVal)) {
                    buttonConfig = getObjectByKeyValue(configData, buttonRow + buttonVal);
                    cout << buttonConfig.dump() << endl;
                    string appName = buttonConfig.at("appName");
                    if (appName == "app") {
                        // button not assigned, ignore
                    } else {
                        focusWindowWithTitle(appName);
                        string command = buttonConfig.at("command");
                        if (command == "sendKeyPress") {
                            string sendKey = buttonConfig.at("parameter");
                            sendKeyPressRelease(charToKey(sendKey[0]));
                        }
                    }
                    buttonMask = invertBitArray(hexStringToBinary(buttonVal));
                    string bckLght = buttonConfig.at("bckLght");
                    if (bckLght == "toggle") {
                        cout << "Toggle: ";
                        if (buttonRow == "1") {
                            toggleButtonLights(buttonRow, row1Bits, buttonMask);
                        } else if (buttonRow == "2") {
                            toggleButtonLights(buttonRow, row2Bits, buttonMask);
                        } else if (buttonRow == "F") {
                            toggleButtonLights(buttonRow, rowFBits, buttonMask);
                        } else {
                            cout << "button row: " + buttonRow + " button val: " + buttonVal + " button mask: " + buttonMask.to_string() << endl;
                        }
                    } else if (bckLght == "momentary") {
                        cout << "Momentary: ";
                        if (buttonRow == "1") {         
                            momentaryButtonLights(buttonRow, row1Bits, buttonMask);
                        } else if(buttonRow == "2") {
                            momentaryButtonLights(buttonRow, row2Bits, buttonMask);
                        } else if(buttonRow == "F") {
                            momentaryButtonLights(buttonRow, rowFBits, buttonMask);
                        }
                    } else if (bckLght == "group") {
                        cout << "Group: ";
                        string maskString = buttonConfig.at("bckLghtMask");
                        bitset<12> groupMask(maskString);
                        if (buttonRow == "1") {
                            groupButtonLights(buttonRow, row1Bits, buttonMask, groupMask);
                        } else if (buttonRow == "2") {
                            groupButtonLights(buttonRow, row2Bits, buttonMask, groupMask);
                        } else if (buttonRow == "F") {
                            groupButtonLights(buttonRow, rowFBits, buttonMask, groupMask);
                        }
                    }
                } 
            }
        }
    }
    cout << "Exiting the program." << endl;
    return 0;
}